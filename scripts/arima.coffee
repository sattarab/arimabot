module.exports = ( arimabot )->
  arimabot.hear /^arimabot$/i, ( msg ) ->
    msg.send "arimabot is pretty cool"

  arimabot.hear /^arima$/i, ( msg ) ->
    msg.send "Arima is a crowd-contributed, open-data knowledge base."

  arimabot.hear /^(arima(\s)*)?(show|show(\s)*more|more)/i, ( msg ) ->
    msg.send "As a participant, you will have the opportunity to browse and answer questions ranging from a variety of topics and gain valuable insights by comparing yourself to others from your area, country and worldwide."

  arimabot.hear /^@(.+)/i, ( msg ) ->
    name = msg.match[1].trim()
    msg.send "Welcome #{ name }, Did you know Shangai is the largest city in the world?"

  arimabot.hear /^(\s)*(give|list)+(\s)*(categories)?/i, ( msg ) ->
    msg.send "(E)ntertainment, (S)ports, (H)ealth"